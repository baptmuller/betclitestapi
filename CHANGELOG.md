### 1.5.0 ###
- feature
- BMR
- feat: add postman collection & readme

### 1.4.0 ###
- feature
- BMR
- docker: add dockerfile

### 1.3.0 ###
- feature
- BMR
- docs: add todo recommendations for prod

### 1.2.0 ###
- feature
- BMR
- feat: add player management and refact routes & models

### 1.1.0 ###
- feature
- BMR
- feat: add tournament management

### 1.0.0 ###
- feature
- BMR
- feat: add the base of the project
