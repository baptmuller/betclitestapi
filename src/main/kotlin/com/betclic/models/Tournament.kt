package com.betclic.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

@Serializable
data class Tournament(val id: Int, val name: String, val players: List<Player>)

class TournamentEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TournamentEntity>(TournamentTable)

    var name by TournamentTable.name
    val players by PlayerEntity referrersOn PlayerTable.tournament
    fun toTournament(): Tournament {
        val sorted = players.map(PlayerEntity::toPlayer).sortedWith(compareBy { it.score }).reversed()
        sorted.map { p -> p.rank = sorted.indexOf(p) + 1 }

        return Tournament(
            id = id.value,
            name = name,
            players = sorted
        )
    }
}

object TournamentTable : IntIdTable() {
    val name = varchar("name", 128)
}