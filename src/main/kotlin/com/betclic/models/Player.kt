package com.betclic.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

@Serializable
data class Player(val id: Int, val pseudo: String, val score: Int, var rank: Int? = null)
class PlayerEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<PlayerEntity>(PlayerTable)

    var pseudo by PlayerTable.pseudo
    var score by PlayerTable.score
    var tournament by TournamentEntity referencedOn PlayerTable.tournament

    fun toPlayer() = Player(
        id = id.value,
        pseudo = pseudo,
        score = score
    )
}

object PlayerTable : IntIdTable() {
    val pseudo = varchar("name", 128)
    val score = integer("score")
    val tournament = reference("tournament", TournamentTable, onDelete = ReferenceOption.CASCADE)
}