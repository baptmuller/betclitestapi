package com.betclic.routes

import com.betclic.dao.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*


fun Route.playerRouting() {
    route("/players") {

        /**
         * @return all the players
         **/
        get {

            call.respond(daoPlayer.allPlayers())
        }

        /**
         * return one player with the id
         * @return the wanted players
         **/
        get("{id}") {

            val id = call.parameters.getOrFail<Int>("id").toInt()
            val player = daoPlayer.getPlayerById(id) ?: return@get call.respondText(
                "Missing or Wrong id",
                status = HttpStatusCode.BadRequest
            )

            call.respond(player)
        }

        /**
         * add a player
         * @return the list of the players updated
         **/
        post {
            val formParameters = call.receiveParameters()
            val pseudo = formParameters.getOrFail("pseudo")
            val score = formParameters.getOrFail("score").toInt()
            val tournamentId = formParameters.getOrFail("tournamentId").toInt()
            val player = daoPlayer.addNewPlayer(pseudo, score, tournamentId)
            call.respondRedirect("/players/${player?.id}")
        }

        /**
         * @param {id} : id of the wanted player
         * @param {_action} : update or delete
         * update or delete a player
         * @return the list of the players updated
         **/
        post("{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val formParameters = call.receiveParameters()
            when (formParameters.getOrFail("_action")) {
                "update" -> {
                    val pseudo = formParameters.getOrFail("pseudo")
                    val score = formParameters.getOrFail("score").toInt()
                    val tournamentId = formParameters.getOrFail("tournamentId").toInt()
                    daoPlayer.editPlayer(id, pseudo, score, tournamentId)
                    call.respondRedirect("/players/$id")
                }
//
//                "delete" -> {
//                    daoPlayer.deletePlayer(id)
//                    call.respondRedirect("/players")
//                }
            }
        }
    }
}