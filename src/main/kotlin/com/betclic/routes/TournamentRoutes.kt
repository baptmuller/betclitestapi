package com.betclic.routes

import com.betclic.dao.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*


fun Route.tournamentRouting() {
    route("/tournaments") {

        /**
         * @return all the tournaments
         **/
        get {

            call.respond(daoTournament.allTournaments())
        }

        /**
         * return one tournament with the id
         * @return the wanted tournaments
         **/
        get("{id}") {

            val id = call.parameters.getOrFail<Int>("id").toInt()
            val tournament = daoTournament.getTournamentById(id) ?: return@get call.respondText(
                "Missing or Wrong id",
                status = HttpStatusCode.BadRequest
            )

            call.respond(tournament)
        }

        /**
         * add a tournament
         * @return the list of the tournaments updated
         **/
        post {
            val formParameters = call.receiveParameters()
            val name = formParameters.getOrFail("name")
            val tournament = daoTournament.addNewTournament(name)
            call.respondRedirect("/tournaments/${tournament?.id}")
        }

        /**
         * @param {id} : id of the wanted tournament
         * @param {_action} : update or delete
         * update or delete a tournament
         * @return the list of the tournaments updated
         **/
        post("{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val formParameters = call.receiveParameters()
            when (formParameters.getOrFail("_action")) {
                "update" -> {
                    val name = formParameters.getOrFail("name")
                    daoTournament.editTournament(id, name)
                    call.respondRedirect("/tournaments/$id")
                }

                "delete" -> {
                    daoTournament.deleteTournament(id)
                    call.respondRedirect("/tournaments")
                }
            }
        }
    }
}