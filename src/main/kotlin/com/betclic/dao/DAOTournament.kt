package com.betclic.dao

import com.betclic.models.*

interface DAOTournament {
    suspend fun allTournaments(): List<Tournament>
    suspend fun getTournamentById(id: Int): Tournament?
    suspend fun addNewTournament(name: String): Tournament?
    suspend fun editTournament(id: Int, name: String): Boolean
    suspend fun deleteTournament(id: Int): Boolean
}