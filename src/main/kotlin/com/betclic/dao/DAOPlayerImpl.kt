package com.betclic.dao

import com.betclic.dao.DatabaseSingleton.dbQuery
import com.betclic.models.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import kotlinx.coroutines.runBlocking

class DAOPlayerImpl : DAOPlayer {
    override suspend fun allPlayers(): List<Player> = dbQuery {
        PlayerEntity.wrapRows(PlayerTable.selectAll()).map(PlayerEntity::toPlayer).toList()
    }

    override suspend fun getPlayerById(id: Int): Player? = dbQuery {
        PlayerEntity.wrapRows(PlayerTable.select { PlayerTable.id eq id }).map(PlayerEntity::toPlayer).singleOrNull()
    }

    override suspend fun addNewPlayer(
        newPseudo: String, newScore: Int, tournamentId: Int
    ): Player = dbQuery {

        val t = TournamentEntity.wrapRows(TournamentTable.select(TournamentTable.id eq tournamentId)).single()

        val newPlayer = PlayerEntity.new {
            pseudo = newPseudo
            score = newScore
            tournament = t
        }

        newPlayer.toPlayer()
    }

    override suspend fun editPlayer(
        id: Int, newPseudo: String, newScore: Int, tournamentId: Int
    ): Boolean = dbQuery {
        PlayerTable.update({ PlayerTable.id eq id }) {
            it[pseudo] = newPseudo
            it[score] = newScore
            it[tournament] = tournamentId
        } > 0
    }

}

val daoPlayer: DAOPlayer = DAOPlayerImpl().apply {
    runBlocking {}
}
