package com.betclic.dao

import com.betclic.dao.DatabaseSingleton.dbQuery
import com.betclic.models.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import kotlinx.coroutines.runBlocking

class DAOTournamentImpl : DAOTournament {

    override suspend fun allTournaments(): List<Tournament> = dbQuery {
        TournamentEntity.wrapRows(TournamentTable.selectAll()).map(TournamentEntity::toTournament).toList()
    }

    override suspend fun getTournamentById(id: Int): Tournament? = dbQuery {
        TournamentEntity.wrapRows(TournamentTable.select { TournamentTable.id eq id })
            .map(TournamentEntity::toTournament).singleOrNull()
    }

    override suspend fun addNewTournament(newName: String): Tournament = dbQuery {
        TournamentEntity.new {
            name = newName
        }.toTournament()
    }

    override suspend fun editTournament(id: Int, name: String): Boolean = dbQuery {
        TournamentTable.update({ TournamentTable.id eq id }) {
            it[TournamentTable.name] = name
        } > 0
    }

    override suspend fun deleteTournament(id: Int): Boolean = dbQuery {
        TournamentTable.deleteWhere { TournamentTable.id eq id } > 0
    }

}

val daoTournament: DAOTournament = DAOTournamentImpl().apply {
    runBlocking {
        if (allTournaments().isEmpty()) {
            addNewTournament("LETS GET READY TO RUMBLEEEE")
        }
    }
}
