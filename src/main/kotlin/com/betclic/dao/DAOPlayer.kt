package com.betclic.dao

import com.betclic.models.*

interface DAOPlayer {
    suspend fun allPlayers(): List<Player>
    suspend fun getPlayerById(id: Int): Player?
    suspend fun addNewPlayer(pseudo: String, score: Int, tournamentId: Int): Player?
    suspend fun editPlayer(id: Int, pseudo: String, score: Int, tournamentId: Int): Boolean
}