package com.betclic

import com.betclic.dao.DatabaseSingleton
import com.betclic.plugins.*
import io.ktor.server.application.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)
fun Application.module() {
    DatabaseSingleton.init()
    configureRouting()
    configureSerialization()
}