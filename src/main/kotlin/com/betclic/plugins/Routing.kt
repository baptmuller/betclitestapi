package com.betclic.plugins

import com.betclic.routes.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {
        tournamentRouting()
        playerRouting()
    }
}
