# TODO pour un déploiement en production #

## Modification de la base de donnée ##

    ici nous utilisons h2 :     

    - Avantages :
        Léger et simple à utiliser.
        Idéal pour le développement et les tests.
    - Inconvénients :
        Les données sont perdues lorsque l'application s'arrête.

    H2 ne conviens donc pas à une utilisation de productions à cause de son stockage de données en mémoire,
    il faudrais s'orienter sur du PostGre ou du MongoDb pour avoir un outil puissant est scalable, 
    ou alors, si notre environnement de production le permet, se greffer aux services AWS en utilisant DynamoDB ;)
    
    - Ajouter les dépendance MongoDB (si on choisit cette option) (build.gradle.kts)
    - Configurer la connexion à MongoDB (Application.kt),
    - Adapter les opérations CRUD pour MongoDB ( modifictions des routes & DAO )
    - Configurer les propriétés de connexion ( var d'env )
    - Tester en environnement de production ( avec examen des perfs et de la sécu )
    - Déploiement 

## Revoir le cycle de vie des Joueurs et leurs rangs ##

    en effet, ici les jouers n'existe que pour les tournois, il faudrait que leurs cycles de vie puissent perdurer 
    pour participer à d'autres tournois, jeux, ou paris

    Pour les rangs des joueurs, ils se calculent à l'affichage de leurs tournois, sans soucis de temps, 
    j'aurais voulu créer une fonction est appelée à chaque appelle CRUD pour re-évaluer le rang des joueurs.
    La liaison entre les tables Player et Tournament par le biai de leurs 'references' dans leurs class m'a rendu 
    la tâche plus longue que prévue et j'ai préféré me focaliser sur autre chose tout en offrant un systeme de rang
    plus simple mais efficace ( re calcul du rang des joueurs à achaque get d'un tournoi )

## Ajouter des tests ##
    Il faudrait aussi ajouter des Tests unitaires pour les fonctions métiers et des tests avec MongoDb en utilisant 
    JUnit ou embedded-mongodb-rules ou mongodb-testcontainers
    
    - Configurer mongodb pour les tests
    - Écrire des tests pour notre API dans test>package>routes
    - Execution des tests avec intégration continue

## L'intégration continue ##
    L'intégration continue manque à ce projet pour être viable en prod, il faudrait configurer des pipelines et des 
    jobs via jenkins
    
    - Installation de jenkins
    - Créez un fichier Jenkinsfile à la racine du projet et definir son contenu
    - Configuration du projet jenkins
    - Paramétrer les variables d'environnement
    - Mettre en place des stages spécifiques pour l'environnement de production
    - Explorez les plugins Jenkins pour Docker, Kubernetes
    
    une bonne configuration de la ci/cd est aussi possible sur gitlab

## Logs et gestions d'erreurs ##
    pour une petite api comme celle la j'aurais pû utiliser Logback avec SLF4J ou Kotlin Logging, mais pour un env de 
    prod, il aurait fallu se diriger vers la suite ELK qui est bien complète, Datadog qui prend très bien en charge ktor
    ou Sentry qui est aussi sympa et prend kotlin en charge
