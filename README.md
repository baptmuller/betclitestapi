# Bonjour et bienvenue sur mon projet kotlin pour réaliser une api avce ktor #

## Clonage du projet ##
    
    Il faudra commencer par cloner le projet avec les commandes suivantes :
    git clone git@gitlab.com:baptmuller/betclitestapi.git
    ou 
    git clone https://gitlab.com/baptmuller/betclitestapi.git

## Build du projet ##

    Ensuite vous pouvez directement build le projet avec la commande docker :
    docker build -t com.betclic.testapi .
    Puis le lancer avec 
    docker run -p 8080:8080 com.betclic.testapi

## Test des routes ##
    
    Comme je n'ai pas réussi a terminer un swagger propre dans les temps (en wip sur la branche feature/add_swagger,
    il manque juste la doc sur les routes pour pouvoir ajouter des params sur l'ui),
    je vous ai glissé un collection postman avec les routes et leurs bodies 
    préremplie. Vous pouvez la retrouver sous le nom de :

[BetclicTestAPI.postman_collection.json](BetclicTestAPI.postman_collection.json)
    
    Ensuite, vous pouvez l'importer directement sur votre application postman (ou l'interface web)
    J'ai essayé de faire un parcours complet en les mettant dans le bon ordre

## Choix techniques ##

- ktor : pour son interopérabilité, sa facilité de configuration et son asynchrone non bloquant
- h2 : pour son intégration facile, ses bonnes perfs sur des bdd légeres et son outil de développement 
- Exposed : pour sa déclaration des schémas, sa très bonne intégration avec ktor 
  et sa compatibilité avec Diverses Bases de Données 